
This is the source distribution for a plagiarism detector.
See the included man page (cheat.1) for details on operation.

You need gmake and gcc/g++ to compile it. You could make do with a
standard make if you rewrite the Makefile.

I've tested it on alpha-OSF1, sun-SunOS4.1.3, mips-ultrix, and
parisc-HPUX.


