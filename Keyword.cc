/*
   Plagiarism detector. (C) 1994 by Trevor Blackwell, Harvard University
*/

#include "config.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <algorithm>
#include <assert.h>
#include "global.h"
#include "Keyword.h"
#include "signature.h"

Keyword *Keyword::keywords;
char **Keyword::current_wl;

Keyword::Keyword(u_int hash, char *name, int space)
{
  left=right=NULL;
  this->hash=hash;
  this->space=space;
  this->name=name;
}

Keyword::Keyword(int space)
{
  name="---";
  hash=1<<29;
  this->space=space;
}

Keyword::~Keyword()
{
  delete left;
  delete right;
}

void
Keyword::add(char *name, int space)
{
  add(hashpjw(name),name,space);
}

Keyword *
Keyword::find(char *name)
{
  return find(hashpjw(name),name);
}

void
Keyword::add(u_int hash, char *name, int space)
{
  int side;

  if (hash>this->hash) side=1;
  else if (hash<this->hash) side=-1;
  else side=strcmp(name,this->name);
    
  switch(side) {
  case 1:
    if (right) {
      right->add(hash,name,space);
    } else {
      right = new Keyword(hash,name,space);
    }
    break;

  case 0:
    break;

  case -1:
    if (left) {
      left->add(hash,name,space);
    } else {
      left = new Keyword(hash,name,space);
    }
    break;
  }
}

Keyword *
Keyword::find(u_int hash, char *name)
{
  int side;

  if (hash>this->hash) side=1;
  else if (hash<this->hash) side=-1;
  else side=strcmp(name,this->name);

  if (side>0) {
    return right?right->find(hash,name):NULL;
  }
  else if (side<0) {
    return left?left->find(hash,name):NULL;
  }
  else {
    return this;
  }
}

void
Keyword::init(char **wl)
{
  // optimization - don't tear down and reconstruct the tree if same word list
  if (wl == current_wl) return;
  current_wl = wl;

  // blow away entire tree (if any), start again
  delete keywords;
  keywords=new Keyword(0);

  for (int i=0; wl[i]; i++) {
    if (wl[i][0]=='@') {
      n_nameSpaces++;
      nameSpaceNames[n_nameSpaces-1]=wl[i]+1;
    } else {
      keywords->add(wl[i],n_nameSpaces-1);
    }
  }
}

void
Keyword::init(FILE *f)
{
  if (!keywords) {
    keywords=new Keyword(0);
  }

  char buf[BUFSIZ];

  while(fgets(buf,BUFSIZ,f)) {
    buf[strlen(buf)-1]=0;  // blow away newline
    if (buf[0]=='@') {
      n_nameSpaces++;
      nameSpaceNames[n_nameSpaces-1]=strdup(buf+1);
    } else {
      keywords->add(buf,n_nameSpaces-1);
    }
  }
}

