/*
   Plagiarism detector. (C) 1994 by Trevor Blackwell, Harvard University
*/

/* 
   Efficient scratch memory system

   Written by Trevor Blackwell, August 1994
*/

#include "config.h"
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include "WorkingMemory.h"

WorkingMemory::WorkingMemory()
{
  allocSoFar=0;
  next=NULL;
  cur=this;
}

WorkingMemory::~WorkingMemory()
{
  delete next;
}

char *
WorkingMemory::primalloc(int size, int align, WorkingMemory **cur)
{
  assert(align>0);

  int start=(allocSoFar+align-1) & ~(align-1);

  char *ret;
  if (start + size >= sizeof(contents)) {
    if (!next) {
      next = new WorkingMemory();
    }
    ret = next->primalloc(size,align,cur);
  } else {
    ret = contents+start;
    allocSoFar = start+size;
    if (cur) *cur = this;
  }    
  return ret;
}

char *
WorkingMemory::strdup(char *s)
{
  char *buf = cur->primalloc(strlen(s)+1,1,&cur);
  strcpy(buf,s);
  return buf;
}

void *
WorkingMemory::alloc(int size)
{
  return (void *)cur->primalloc(size,sizeof(long),&cur);
}
