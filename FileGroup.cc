/*
   Plagiarism detector. (C) 1994-1999 by Trevor Blackwell, Harvard University
*/

#include "config.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <algorithm>
#include <vector>

#include "global.h"
#include "FileGroup.h"
#include "signature.h"

FileGroup::FileGroup()
{
}

void
FileGroup::add_file(char *name)
{
  // They may be less long than argc. but argc is an upper bound


  FILE *f=fopen(name,"r");
  if (!f) {
    perror(name);
    return;
  }
    
  if (printMunged) {
    fprintf(stderr,"%s:\n",name);
  }
  TextSignature *it = new TextSignature;
  it->filename = strdup(name);
  it->groupid = ngroups;
  it->readFile(f);
  fclose(f);

  files.push_back(it);
  if (verbose && !(files.size()%10)) fprintf(stderr,"."); 

}

struct comp_fps {
  bool operator() (const FilePairScore &a, const FilePairScore &b) {
    return a.score > b.score;
  }
};

void
FileGroup::computeCorr()
{
  int i,j;

  int nsets=files.size();

  correl_t *correls = new correl_t[nsets*nsets];
  TextSignature::computeAllCorrelations(files,correls);

  if (verbose) fprintf(stderr,"Tally...");
  for (i=0; i<nsets; i++) {
    for (j=i+1; j<nsets; j++) {

      if (ngroups>1 && files[i]->groupid == files[j]->groupid) continue;
      double ht = correls[i*nsets+j];
      if (ht<0.001) continue; // ex-recto

      FilePairScore it;
      it.i=i;
      it.j=j;
      it.score = ht;
      corr.push_back(it);

      for (int h=0; h<NHIGHERTHAN; h++) {
        if (ht >= 1.0) {
          higherThan[h]++;
        }
        ht *= 2.0;
      }
    }
  }
  delete [] correls;

  if (verbose) fprintf(stderr,"sort %d...", int(corr.size()));
  sort(corr.begin(), corr.end(), comp_fps());

  if (verbose) fprintf(stderr,"done\n");

  fprintf(stderr,"%d files in %d groups => %d correlations\n",nsets,ngroups,int(corr.size()));
}

void
FileGroup::printResult()
{

  printf("   score  files\n");
  for (int ind=0; ind<corr.size() && ind<topToPrint; ind++) {
    
    printf("%8.4f  %s\n          %s\n",
	   corr[ind].score,
	   files[corr[ind].i]->filename,files[corr[ind].j]->filename);
  }
  double ht=1.0;
  for (int h=1; h<NHIGHERTHAN; h++) {
    printf("Correlations higher than 2^-%-2d: %d\n",h,higherThan[h]);
    ht *= 0.5;
  }
  printf("Correlations higher than 0    : %d\n", int(corr.size()));
}

FileGroup::~FileGroup()
{
}
