/*
   Plagiarism detector. (C) 1994 by Trevor Blackwell, Harvard University
*/

#ifndef GLOBAL_H
#define GLOBAL_H

extern int verbose;
extern int topToPrint;

typedef enum {
  TreatIgnore,
  TreatExistence,
  TreatMatch
} Treatment;

extern char const *TreatmentNames[];


#define MAX_NAMESPACES 100
extern int n_nameSpaces;
extern Treatment treatName[MAX_NAMESPACES];
extern char const *nameSpaceNames[MAX_NAMESPACES];
extern Treatment treatNum;
extern Treatment treatWhite;
extern Treatment treatComment;
extern Treatment treatString;
extern Treatment treatSymbol;

extern int c_style_comments;
extern int asm_style_comments;
extern int lisp_style_comments;

extern int printMunged;

extern int expectedClones;

extern double falloff;

#endif
