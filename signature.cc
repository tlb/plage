/*
   Plagiarism detector. (C) 1994-1999 by Trevor Blackwell, Harvard University
*/

#include "config.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <assert.h>
#include <algorithm>
#include <vector>
#include "global.h"
#include "signature.h"
#include "Keyword.h"

using namespace std;

/*
   Create a new TextSignature, empty
*/
TextSignature::TextSignature()
{
  tokens=NULL;
  numTokens=0;
}

TextSignature::~TextSignature()
{
  delete tokens;
}

/*
   This horrible stuff defines a very efficient way of computing mod
   MODULUS.  The function modulate reduces its argument to be within
   0..MODULUS-1.  MODULUS is chosen such that we will always stay
   within the size of a long, for the calculations we do.

   Uses bigger primes (hence fewer spurious collisions) on 64-bit machines.

   FACTOR must be a power of 2.
*/
#ifdef __alpha__
#define MODULUS_LEN 45
#define MODULUS_SUB 55
#define FACTOR 65536L
#else
#define MODULUS_LEN 19
#define MODULUS_SUB 1
#define FACTOR 2048L
#endif

// a prime. Must be multipliable by 2*FACTOR and stay within a long
#define MODULUS ((1L<<MODULUS_LEN)-MODULUS_SUB)  

// heavily based on properties of MODULUS
inline void modulate(u_long &x) {
  x += FACTOR*MODULUS;         // make sure it's positive
  u_long y = x>>MODULUS_LEN;     
  x = (x&((1L<<MODULUS_LEN)-1)) + MODULUS_SUB*y;
  if (x>=MODULUS) x-=MODULUS;
}

/* From Aho, Sethi, Ullman */
u_int hashpjw(char *x)
{
  u_int h=0;
  while (*x) {
    h=(h<<4)+*x;
    u_int g=h&0xf0000000;
    h=(h^(g>>24))^g;
    x++;
  }
  return h;
}

/*
   Read a string, and add its signature elements to the hash table. No
   processing is done on the string. 

   Uses the Rabin-Karp algorithm to compute the n hashes in O(n) time,
   independent of the length of the substrings.

   Hashes at the beginning of the file are computed as if there were
   an infinite prefix of 0 bytes.
*/
void 
TextSignature::addHashes(SignatureRecord **putptr, int fromIndex, int hashLen)
{
  int i;

  short *history = new short[hashLen];

  int bufind;
  u_long subfac = 1;
  
#if 0   /* This needs bigger math */
  for (i=31; i>=0; i--) {
    subfac = subfac * subfac;
    if (len & (1<<i)) {
      subfac *= FACTOR;
    }
    modulate(subfac);
  }
#else
  for (i=0; i<hashLen; i++) {
    subfac *= FACTOR;
    modulate(subfac);
  }
#endif
  
  for (i=0; i<hashLen; i++) {
    history[i]=0;
  }

  bufind=0;
  u_long sum=0;

  short *p1=tokens;
  for (i=0; i<numTokens; i++, p1++) {
    // For each character in the text, let sum =
    // (sum*FACTOR+character)%MODULUS, a la Rabin-Karp. So this
    // computes a hash function for _each_ position in the text,
    // using only a few operations per character. 
    sum *= FACTOR;

    u_long val = ((unsigned short)*p1)&(FACTOR-1);
    
    sum = sum + val - subfac * history[bufind];
    modulate(sum);

    (*putptr)->hash = (u_int)(sum?sum:1L);
    (*putptr)->fromIndex = fromIndex;
    (*putptr)++;

    history[bufind] = val;
    bufind = (bufind+1) & (hashLen-1);
  }
  delete [] history;
}

int
TextSignature::numHashes(int hashLen)
{
  return numTokens;
}

void
TextSignature::readFile(FILE *f)
{
  rewind(f);
  const int BSIZE=500000;
  // We make buf static, so we don't keep allocating it and deallocating 
  // it, since that seems to cause hideous fragmentation on certain
  // platforms. If the file is longer than BSIZE bytes, then we
  // truncate. Maybe this should be improved.
  static char *buf;

  if (!buf) buf = new char [BSIZE];

  int len=read(fileno(f),buf,BSIZE);
  buf[len]=0;

  readText(buf);
}


void
TextSignature::readText(char *ibuf)
{
  int i;
  tokens = new short[strlen(ibuf)*2+1];  // upper bound on length
  char *ip=ibuf;
  short *op=tokens;
  const int KWLOOKBACK=20;
  u_int lookback[KWLOOKBACK];

  for (i=0; i<KWLOOKBACK; i++) {
    lookback[i]=0;
  }

  while (*ip) {

    u_int shiftin=0;

    if (isdigit(*ip) || *ip=='-') {

      char *startNum=ip;
      while (isdigit(*ip) || *ip=='-' || *ip=='e' || *ip=='E') ip++;

      char lastchar = ip[0];
      ip[0]=0;
      u_int hash=atoi(startNum);

      switch(treatNum) {
      case TreatIgnore:
	      break;
      case TreatExistence:
        *op++ = 22000;
        if (printMunged) fprintf(stderr,"e(num) ");
        shiftin=1;
        break;
      case TreatMatch:
        *op++ = hash;
        if (printMunged) fprintf(stderr,"eh(%s) ",startNum);
        shiftin=1;
        break;
      }

      ip[0]=lastchar;
    }

    else if (isalpha(*ip) || *ip=='_' || *ip=='$') {

      /* Turn word into a single token */
      char *startName=ip;
      ip++;
      while (isalnum(*ip) || *ip=='_') ip++;

      char lastchar = ip[0];
      ip[0]=0;
      u_int nhash = hashpjw(startName);

      short hash = nhash&16383;
      int space;

      Keyword *k = Keyword::keywords->find(startName);
      if (k) {
        space=k->space;
      } else {

        space=0; // new
        
        for (i=1; i<KWLOOKBACK; i++) {
          if (lookback[i]==nhash) {
            hash = 20000+i;
            space=1; // recent
            break;
          }
        }
      }
	  
      assert(space>=0 && space<n_nameSpaces);
      switch (treatName[space]) {
      case TreatIgnore:
        break;
            case TreatExistence:
        if (printMunged) fprintf(stderr,"e(name.%d %s) ",space,startName);
        *op++ = 25000+space;
        shiftin=1;
        break;
            case TreatMatch:
        if (printMunged) fprintf(stderr,"c(name.%d %s=%d) ",space,startName,hash);
        *op++ = hash;
        shiftin=nhash;
        break;
      }

      lookback[0]=nhash;
      ip[0]=lastchar;
    }

    else if (c_style_comments && ip[0]=='/' && ip[1]=='*') {

      /* Skip entire comment */
      ip+=2;
      char *startComment = ip;
      while (!(ip[0]=='*' && ip[1]=='/') && ip[0]!=0) ip++;
      if (*ip) ip++;
      if (*ip) ip++;

      switch (treatComment) {

        case TreatIgnore:
          break;

        case TreatExistence:
          if (printMunged) fprintf(stderr,"e(comment) ");
          *op++ = 30000;
          shiftin=1;
          break;

        case TreatMatch:
          char lastchar = *ip;
          *ip = 0;
          *op++ = hashpjw(startComment)&16383;
          if (printMunged) fprintf(stderr,"h(%s) ",startComment);
          *ip = lastchar;
          shiftin=1;
          break;
      }
    }

    else if (c_style_comments && ip[0]=='/' && ip[1]=='/') {

      /* Skip entire comment */
      ip+=2;
      char *startComment = ip;
      while (!(ip[0]=='\n') && ip[0]!=0) ip++;
      if (*ip) ip++;

      switch (treatComment) {

        case TreatIgnore:
          break;

        case TreatExistence:
          if (printMunged) fprintf(stderr,"e(comment) ");
          *op++ = 30000;
          shiftin=1;
          break;

        case TreatMatch:
          char lastchar = *ip;
          *ip = 0;
          *op++ = hashpjw(startComment)&16383;
          if (printMunged) fprintf(stderr,"h(%s) ",startComment);
          *ip = lastchar;
          shiftin=1;
          break;
      }
    }

    else if (asm_style_comments && ip[0]=='#') {

      /* Skip entire comment */
      ip++;
      char *startComment = ip;
      while (!(ip[0]=='\n') && ip[0]!=0) ip++;
      if (*ip) ip++;

      switch (treatComment) {

        case TreatIgnore:
          break;

        case TreatExistence:
          if (printMunged) fprintf(stderr,"e(comment) ");
          *op++ = 30000;
          shiftin=1;
          break;

        case TreatMatch:
          char lastchar = *ip;
          *ip = 0;
          *op++ = hashpjw(startComment)&16383;
          if (printMunged) fprintf(stderr,"h(%s) ",startComment);
          *ip = lastchar;
          shiftin=1;
          break;
      }
    } 

    else if (lisp_style_comments && ip[0]==';') {

      /* Skip entire comment */
      ip++;
      char *startComment = ip;
      while (!(ip[0]=='\n') && ip[0]!=0) ip++;
      if (*ip) ip++;

      switch (treatComment) {

        case TreatIgnore:
          break;

        case TreatExistence:
          if (printMunged) fprintf(stderr,"e(comment) ");
          *op++ = short(30000);
          shiftin=1;
          break;

        case TreatMatch:
          char lastchar = *ip;
          *ip = 0;
          *op++ = hashpjw(startComment)&16383;
          if (printMunged) fprintf(stderr,"h(%s) ",startComment);
          *ip = lastchar;
          shiftin=1;
          break;
      }
    } 

    else if (isspace(*ip)) {

      /* Skip spaces */
      char *startSpace=ip;

      while (isspace(*ip)) ip++;

      switch (treatWhite) {

        case TreatIgnore:
        break;

        case TreatExistence:
          *op += short(35000);
          if (printMunged) fprintf(stderr,"e(space) ");
          shiftin=1;
          break;

        case TreatMatch:
          *op = ip-startSpace;
          if (printMunged) fprintf(stderr,"h(%d spaces) ", int(ip - startSpace));
          shiftin=1;
        break;
      }
    } 

    else if (*ip == '"') {

      /* Skip contents */

      ip++;
      char *startString=ip;
      if (*ip=='\\') ip+=2;
      while (!(ip[0]=='"') && ip[0] != '\n' && ip[0]) {
	      ip++;
	      if (ip[0]=='\\' && ip[1]) ip+=2;
      }
      if (*ip) ip++;
      
      switch (treatString) {

        case TreatIgnore:
          break;

        case TreatExistence:
          *op += short(40000);
          if (printMunged) fprintf(stderr,"e(string) ");
          shiftin=1;
          break;

        case TreatMatch:
          char lastchar = *ip;
          *ip = 0;
          *op = hashpjw(startString)&16383;
          if (printMunged) fprintf(stderr,"h(%s) ",startString);
          *ip = lastchar;
          shiftin=1;
          break;
      }
    } 

    else {
      /* Other characters */
        switch (treatSymbol) {

        case TreatIgnore:
          break;

        case TreatExistence:
          if (printMunged) fprintf(stderr,"e(symbol) ");
          *op++ = short(45000);
          shiftin=1;
          break;

        case TreatMatch:
          *op++ = short(*ip);
          if (printMunged) fprintf(stderr,"h(%c) ",*ip);
          shiftin=1;
          break;
      }

      ip++;

    }
    if (shiftin != 0) {
      for (i=KWLOOKBACK-2; i>=0; i--) {
	      lookback[i+1]=lookback[i];
      }
      lookback[0]=shiftin;
    }
  }

  assert(ip-ibuf == strlen(ibuf));

  numTokens = (int)(op-tokens);
  tokens = (short *) realloc(tokens,sizeof(short)*numTokens+1);
  if (printMunged) fprintf(stderr,"\n");
}


/*
   This helps us sort signature records so that identical hashes go
   together, and within that, the fromIndices go in order. It's
   important to have the fromIndices in order because we have to cope
   with the case that a single hash element appears multiple times in
   a single TextSignature.
*/
static int
compare_SignatureRecord(const void *xa, const void *xb)
{
  SignatureRecord *a = (SignatureRecord *)xa;
  SignatureRecord *b = (SignatureRecord *)xb;

  if (a->hash < b->hash) return -1;
  if (a->hash > b->hash) return 1;
  if (a->fromIndex < b->fromIndex) return -1;
  if (a->fromIndex > b->fromIndex) return 1;
  return 0;
}

/*
   Efficiently compute the cross-correlation between every pair of
   TextSignatures in sigs.

   We do this by creating a SignatureRecord object for each signature
   element found in each TextSignature. We then sort them by hash, so
   it's easy to find SignatureRecords with the same hash.

   When we have this nice sorted list of SignatureRecords, we can
   traverse through it. For every group with the same hash, we
   increment the cross-correlation of every pair. Except, we don't do
   this if there are more than expectedClones members of the group:
   the idea being that this signature element represents some sort of
   system-wide boilerplate that doesn't represent cheating.
*/
void 
TextSignature::computeAllCorrelations(vector<TextSignature *> &files,
				      float *correls)
{
  int i,j;
  int hcount=0;
  if (verbose) fprintf(stderr,"Preparing for correlating...");

  int nsigs = files.size();

  /* Zero out correlation array */
  for (i=0; i<nsigs; i++) {
    for (j=0; j<nsigs; j++) {
      correls[nsigs*i+j]=0.0;
    }
  }

  if (verbose) fprintf(stderr,"done\n");

  
  double fallen=0.0;
  for (int hashLen=8; hashLen <= 64; hashLen *= 2) {

    if (verbose) fprintf(stderr,"Len %d...",hashLen);
    hcount=0;
    for (i=0; i<nsigs; i++) {
      hcount += files[i]->numHashes(hcount);
    }

    if (verbose) fprintf(stderr,"new %d...",hcount);

    SignatureRecord *recs = new SignatureRecord[hcount];
    SignatureRecord *putptr = recs;

    for (i=0; i<nsigs; i++) {
      files[i]->addHashes(&putptr,i,hashLen);
    }

    assert(putptr <= recs+hcount);

    if (verbose) fprintf(stderr,"sort %d...",hcount);

    qsort((void *)recs,
	  hcount,
	  sizeof(SignatureRecord),
	  compare_SignatureRecord);

    if (verbose) fprintf(stderr,"uniq...");

    /* Remove duplicate hashes from same source file */
    j=0;
    u_int lastHash=0;
    int lastFI=-1;
    for (i=0; i<hcount; i++) {
      if (recs[i].hash != lastHash || recs[i].fromIndex != lastFI) {
	      recs[j++] = recs[i];
      }
      lastHash=recs[i].hash;
      lastFI=recs[i].fromIndex;
    }
    hcount=j;
    
    if (verbose) fprintf(stderr,"aug %d...",hcount);

    double expectation = 1.0/expectedClones;
    
    int boilerCount=0;
    int uniqueHashCount=0;
    int count=0;
    for (i=0; i<hcount; i+=count) {
      for (count=0; 
        i+count < hcount && recs[i].hash==recs[i+count].hash; 
        count++) {}

      // Now there are count identical hashes, from i..i+count-1. We
      // need to bump the correlations of all pairs.
      if (count < expectedClones) {
        for (int c1=0; c1<count; c1++) {
          for (int c2=c1+1; c2<count; c2++) {
            int i1=recs[i+c1].fromIndex;
            int i2=recs[i+c2].fromIndex;
            assert(i1<i2);
            correls[nsigs*i1+i2]+= 1.0;
          }
        }
      } else {
	      boilerCount++;
      }
      uniqueHashCount++;
    }

    delete [] recs;

    if (verbose) fprintf(stderr,"bp %d/%d...",boilerCount,uniqueHashCount);
    if (verbose) fprintf(stderr,"fall...");
    fallen += 1.0;
    {
      double ifalloff = 1.0/falloff;
      for (int i1=0; i1<nsigs; i1++) {
        for (int i2=i1+1; i2<nsigs; i2++) {
          correls[nsigs*i1+i2] *= ifalloff;
        }
      }
      fallen *= ifalloff;
    }
    if (verbose) fprintf(stderr,"done\n");
  }
  {
    for (int i1=0; i1<nsigs; i1++) {
      for (int i2=i1+1; i2<nsigs; i2++) {
        correls[nsigs*i1+i2] *= 1.0/(fallen*max(1,max(files[i1]->numHashes(1),
						      files[i2]->numHashes(1))));
      }
    }
  }
}


