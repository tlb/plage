.TH PLAGE 1
.SH NAME
plage \- detect copying of code among many source files
.SH SYNOPSIS
.B plage 
[-top howmany] 
[-ec expectedClones] 
[-fa falloff] 
[-v] 
[-pm]
[-white treatment] 
[-comment treatment] 
[-name space treatment]
[-string treatment] 
[-symbol treatment] 
[-num treatment] 
[-ccomments {0,1}] 
[-asmcomments {0,1}] 
[-lispcomments {0,1}]
[file | -flist filename].. [, files...]

where treatment={Ignore,Existence,Contents}\n",argv[0]);

.SH DESCRIPTION
.I plage
computes a signature function on each file given. Compares each pair
of files to detect commonality in their signatures, and computes a
score for each. Prints out the highest scoring pairs, which should be
manually inspected for evidence of plagiarism. 

If you provide a list of filenames with no commas, similarities
between all pairs of files are detected. If you group files using
commas, only similarities between files in different groups are
reported. For student assignments, you should do something like:

.RS
plage fred/f1.c fred/f2.c , joe/f1.c joe/f2.c ...
.RE

to detect any similarities between Fred's and Joe's files, but not
between two files of the same student.

Plage can also be used to detect cloning in libraries, where it merely
constitutes questionable software design, rather than illegal
behaviour.

Running time is approximately O(n log n), where n is the aggregate
size of all files.  

For large numbers of files, use -flist filename instead of listing
files if the list might be too long for the command line. For example,

.RS
find /sco/src -name '*.[ch]' >sco-files
.RE
.RS
find /linux/src -name '*.[ch]' >linux-files
.RE
.RS
plage -flist sco-files , -flist linux-files
.RE

It prints out the pairs of files with highest correlation scores. The
correlation score by itself doesn't prove or disprove anything -- it's
merely an indication that you should look at the files to see what has
been copied.

The signature function on a given file is computed by taking all
substrings of length 16, 32, and 64, and computing a hash sum for
each. Then, a set of files can be compared for similarities by
matching up identical signature elements (the hash sums).

By default, the following bits of code are considered equivalent:

.RS
for (x=0; x<10; x++) printf("hello");
.RE
.RS
for (a=0; a<10; a++) printf("foo");
.RE
.RS
for(a=0;a<10;a++) /* loop */ 
.RS
 printf("hello");
.RE
.RE

but the following are all different:

.RS
if (x<10)
.RE
.RS
if (x>10)
.RE
.RS
if (x<100)
.RE
.RS
while (x<10)
.RE

and the following are also different:

.RS
printf("foo")
.RE
.RS
puts("foo")
.RE
.RS
printf(str)
.RE

It does this by identifying variable names, keyword names, strings,
and numbers and comments and treating them separately. It turns out
that this is mostly language independent, but see the 
.B -asmcomments,
.B -pycomments,
.B -perlcomments,
.B -lispcomments,
options if you are using assembly or lisp.

You can change what tokens are considered different or equivalent. For
example, if you think that changing numbers doesn't change the nature
of the code, then use
.B -num Existence
to indicate that only the presence of a number, not its actual value,
should be indicated in the tokenization. If you think similar code
with changed variable names is not plagiarism, then use
.B -name new Contents.

Names are the most complex part of detecting similarities. It knows
what names are likely to be universal (while, main) and which are not
(x, foo). It does this by having a huge list of names found in
standard header files and the keywords from C.

How is this done? The input is first separated into tokens, where each
token is one of the following types:
.TP 
.B number
.TP
.B whitespace
.TP
.B comment
(/* example */). Depends on settings of
.B -asmcomments,
.B -lispcomments,
and
.B -ccomments.
.TP
.B new
(an alphanumeric token that hasn't appeared recently)
.TP
.B recent
(an alphanumeric token that has)
.TP
.B c-keywords
(for, while, ...).
.TP
.B pascal-keywords
(begin, integer)
..TP
.B asm-keywords
(xor, mult)
.TP
.B c-common
(copyright, main)
.TP
.B libc
(strdup, read)
.TP
.B c-defines
(ELOOP, FCREAT)

.RE
The treatments, and the conditions for which they consider two bits of
code to be equivalent are:
.TP
.B ignore
A reasonable treatment for whitespace and comments, such tokens are
simply stripped from the output
.TP
.B existence
A token of the same class must exist.
.TP
.B match
Tokens must match exactly.
.RE

The defaults are reasonable; run plage -v to see them.

.SH OPTIONS
.TP
.B \-top n
Print out the top n offenders. Default 50.
.TP
.B \-boiler n
Signature elements which appear in more than n files are
ignored. Default is 3+#files/20. This is to avoid detecting global
boilerplate
.TP
.B \-fa x
Amount by which to reduce the contribution to the score of signature
elements from the shorter substrings. 3 substring lengths are
considered: 16, 32, and 64. The longest string is weighted as 1, the
second longest as 1/x, the third longest as 1/(x^2), etc. Default is
1.5.
.TP
.B \-white treatment
Control how whitespace is treated.
.TP
.B \-comment treatment
Control how comments are treated.
.TP
.B \-name namespace treatment
Control how names in
.I namespace
are treated. The known namespaces are:
.I new
.I recent
.I c-keywords,
.I pascal-keywords,
.I asm-keywords,
.I c-common,
.I libc,
.I c-defines,
.I roberts,
.TP
.B \-symbol treatment
Control how symbolic operators (like +) are treated.
.TP
.B \-num treatment
Control how numbers are treated.
.TP
.B \-string treatment
Control how strings are treated
.TP
.B \-ccomments {0,1}
enable c-style comments, including both \/* .. *\/ and \/\/ .. end of line
.TP
.B \-asmcomments {0,1}
enable assembly-style comments, meaning # .. end of line
.TP
.B \-lispcomments {0,1}
enable assembly-style comments, meaning \; .. end of line
.TP
.B \-pm
Print munged data (after all above transformations).
Use this with only a few files.
.TP
.B \-v
Verbose mode. Print out parameters and statistics.

.SH BUGS

The command syntax is a bit opaque.

The amount of memory it uses could probably be reduced. There is a
large upper-triangular matrix for which we store the whole square for
easy indexing.

It would be helpful to have a separate utility that looks at two files
in more detail, and highlights the similarities. Perhaps Emacs ediff
could be harnessed for this purpose.

It claims to handle several languages equally, but C is handled more
equally than the others.

.SH COPYRIGHT
Copyright 1994-2003 by Trevor Blackwell <tlb@tlb.org>.
