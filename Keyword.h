/*
   Plagiarism detector. (C) 1994 by Trevor Blackwell, Harvard University
*/

#include <sys/types.h>


class Keyword {
 public:
  void add(char *name, int space);
  Keyword *find(char *name);
  Keyword(int space);  // produce root
  ~Keyword();

  static void init(char **);
  static void init(FILE *);

 private:
  Keyword(u_int hash, char *name, int space);
  void add(u_int hash, char *name, int space);
  Keyword *find(u_int hash, char *name);

 public:
  u_int hash;
  int space;
  char const *name;

 private:
  struct Keyword *left;
  struct Keyword *right;

 public:
  static Keyword *keywords;
  static char **current_wl;
};

