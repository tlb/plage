#ifndef _CHEAT_SIGNATURE_H
#define _CHEAT_SIGNATURE_H
/*
   Plagiarism detector. (C) 1994 by Trevor Blackwell, Harvard University
*/

/*
   This class defines a signature function on a piece of text, and an
   efficient method of finding signatures with some similarity.

   It does this by computing a hash sum for every substring of lengths
   2^(BASE_LENGTH..BASE_LENGTH+NUM_LENGTHS-1).
   
   The hash sums are kept in sorted_hashes array, in ascending order.

   Calling readFile causes it to read in (another) file, and add this
   to its set of hash sums. It looks at the globals mergeWhite and
   smashAlpha when doing this, do modify the way it munges the file
   before computing the hash sums.

   Once you have created a TextSignature instance for every file you
   care about, you can then call computeAllCorrelations, and give it a
   list of TextSignature objects, and it will fill in the correls
   array with the cross-correlation between every pair of
   signatures. It looks at the global variable expectedClones and
   falloff when doing this, to adjust its algorithm.

   See comments in signature.cc for implementation descriptions
*/

#include <sys/types.h>
#include <vector>

using namespace std;

/*
   This defines a hash function, and where it came from.
*/
class SignatureRecord {
 public:
  u_int hash;
  int fromIndex;
};

typedef float correl_t;

struct TextSignature {
  TextSignature();
  ~TextSignature();

  /* Bring in new text */
  void readFile(FILE *f);
  void readText(char *buf);

  /* Add my hashes to *putptr, advancing it */
  void addHashes(SignatureRecord **putptr, int fromIndex, int hashLen);
  int numHashes(int hashLen);

  char *filename;
  int groupid;
  int numTokens;
  short *tokens;

  /* 
     In one big operation, compute all cross correlations.
     Correls is an upper-triangluar array, such that
     correls[i*nsigs+j], i<j, measures the similarity between 
     sig[i] and sig[j]. It should be allocated as correl_t[nsigs*nsigs];
  */
  static void computeAllCorrelations(vector<TextSignature *> &files,
				     correl_t *correls);
};

u_int hashpjw(char *x);


#endif
