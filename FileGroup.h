/*
   Plagiarism detector. (C) 1994 by Trevor Blackwell, Harvard University
*/

#include "signature.h"


struct FilePairScore {
  float score;
  unsigned short i,j;
};

class TokenSet;
class TextSignature;

struct FileGroup {
 public:
  FileGroup();
  ~FileGroup();
  void printResult();

  void add_file(char *name);

  vector<TextSignature *> files;

  int ngroups;
  vector<FilePairScore> corr;

  enum {NHIGHERTHAN=15};
  int higherThan[NHIGHERTHAN];

  void computeCorr();
};
