/*
   Plagiarism detector. (C) 1994 by Trevor Blackwell, Harvard University
*/

#include "config.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <algorithm>
#include <vector>

#include "global.h"
#include "FileGroup.h"
#include "Keyword.h"

// externed in global.h
int verbose;
int topToPrint;

int n_nameSpaces;
char const *nameSpaceNames[MAX_NAMESPACES];

Treatment treatWhite;
Treatment treatNum;
Treatment treatName[MAX_NAMESPACES];
Treatment treatComment;
Treatment treatString;
Treatment treatSymbol;

int c_style_comments;
int asm_style_comments;
int lisp_style_comments;

int expectedClones;
int printMunged;
double falloff;

char const *TreatmentNames[]={
  "ignore",
  "existence",
  "match",
  0,
};

extern char *words_list[];

Treatment
nameToTreatment(char *name)
{
  for (int i=0; TreatmentNames[i]; i++) {
    if (!strcmp(name,TreatmentNames[i])) {
      return (Treatment)i;
    }
  }
  fprintf(stderr,"Invalid treatment: %s\n",name);
  return TreatIgnore;
}


char const *copyright = "Copyright 1994-2003 by Trevor Blackwell <tlb@tlb.org>";

int main(int argc, char **argv)
{
  topToPrint=50;
  verbose=0;

  n_nameSpaces=2; // because 0 is none, 1 is 'previous'
  nameSpaceNames[0]="new";
  nameSpaceNames[1]="recent";

  treatWhite=TreatIgnore;
  treatComment=TreatIgnore;
  treatNum=TreatMatch;

  treatName[0]=TreatExistence;
  treatName[1]=TreatMatch;
  for (int i=2; i<MAX_NAMESPACES; i++) {
    treatName[i]=TreatMatch;
  }

  treatString=TreatExistence;
  treatSymbol=TreatMatch;

  c_style_comments=1;
  asm_style_comments=0;
  lisp_style_comments=0;

  printMunged=0;
  falloff=1.5;

  Keyword::init(words_list);

  if (argc==1) {
    fprintf(stderr,"Syntax: %s [-top howmany] [-boiler boilerplate_threshold] [-fa falloff] [-v] [-pm]\n"
	           "        [-white treatment] [-comment treatment] [-name space treatment]\n"
	           "        [-string treatment] [-symbol treatment] [-num treatment]\n"
	           "        [-ccomments {0,1}] [-asmcomments {0,1}] [-lispcomments {0,1}] [files..]\n"
	           "        where treatment={ignore,existence,match}\n",argv[0]);
    exit(2);
  }
  argc--; argv++; // skip invocation name

  if (verbose) {
    fprintf(stderr,"Cheat. %s\n",copyright);
  }

  expectedClones=0;

  FileGroup *fg = new FileGroup();

  while (argc>0) {
    if (argc>=2 && !strcmp(argv[0],"-top")) {
      topToPrint=atoi(argv[1]);
      argc-=2; argv+=2;
      continue;
    }
    if (argc>=2 && !strcmp(argv[0],"-kw")) {
      FILE *f=fopen(argv[1],"r");
      if (!f) {
	perror(argv[1]);
	exit(1);
      }
      Keyword::init(f);
      fclose(f);
      argc-=2; argv+=2;
      continue;
    }
    if (argc>=2 && !strcmp(argv[0],"-ec")) {
      expectedClones=atoi(argv[1]);
      argc-=2; argv+=2;
      continue;
    }
    if (argc>=2 && !strcmp(argv[0],"-fa")) {
      falloff=atof(argv[1]);
      argc-=2; argv+=2;
      continue;
    }
    if (!strcmp(argv[0],"-v")) {
      verbose++;
      argc-=1; argv+=1;
      continue;
    }
    if (!strcmp(argv[0],"-q")) {
      verbose--;
      argc-=1; argv+=1;
      continue;
    }
    if (!strcmp(argv[0],"-pm")) {
      printMunged=1;
      argc-=1; argv+=1;
      continue;
    }
    if (argc>=2 && !strcmp(argv[0],"-ccomments")) {
      c_style_comments = atoi(argv[1]);
      argc-=2; argv+=2;
      continue;
    }
    if (argc>=2 && !strcmp(argv[0],"-asmcomments")) {
      asm_style_comments = atoi(argv[1]);
      argc-=2; argv+=2;
      continue;
    }
    if (argc>=2 && !strcmp(argv[0],"-lispcomments")) {
      lisp_style_comments = atoi(argv[1]);
      argc-=2; argv+=2;
      continue;
    }
    if (argc>=2 && !strcmp(argv[0],"-white")) {
      treatWhite = nameToTreatment(argv[1]);
      argc-=2; argv+=2;
      continue;
    }
    if (argc>=2 && !strcmp(argv[0],"-num")) {
      treatNum = nameToTreatment(argv[1]);
      argc-=2; argv+=2;
      continue;
    }
    if (argc>=2 && !strcmp(argv[0],"-comment")) {
      treatComment = nameToTreatment(argv[1]);
      argc-=2; argv+=2;
      continue;
    }
    if (argc>=3 && !strcmp(argv[0],"-name")) {
      int space;
      if (isdigit(argv[1][0])) {
	space=atoi(argv[1]);
      } else {
	for (space=0; space<n_nameSpaces; space++) {
	  if (!strcmp(argv[1],nameSpaceNames[space])) break;
	}
	if (space==n_nameSpaces) {
	  fprintf(stderr,"Invalid namespace name\n");
	  exit(2);
	}
      }
      assert(space>=0 && space<n_nameSpaces);
      treatName[space] = nameToTreatment(argv[2]);
      argc-=3; argv+=3;
      continue;
    }
    if (argc>=2 && !strcmp(argv[0],"-string")) {
      treatString = nameToTreatment(argv[1]);
      argc-=2; argv+=2;
      continue;
    }
    if (argc>=2 && !strcmp(argv[0],"-symbol")) {
      treatSymbol = nameToTreatment(argv[1]);
      argc-=2; argv+=2;
      continue;
    }
    if (argc>=2 && !strcmp(argv[0],"-flist")) {
      FILE *fp=fopen(argv[1], "r");
      if (!fp) {
        perror(argv[1]);
        exit(1);
      }
      while (1) {
        char line[1024];
        if (!fgets(line, sizeof(line), fp)) break;
        int l=strlen(line);
        if (l>0 && line[l-1]=='\n') line[--l]=0;
        fg->add_file(line);
      }
      fclose(fp);
      argc-=2; argv+=2;
      continue;
    }
    
    if (!strcmp(argv[0],",")) {
      fg->ngroups++;
      argc--; argv++;
      continue;
    }

    fg->add_file(argv[0]);
    argc--; argv++;
  }

  if (verbose) {
    printf("Parms: \n");
    printf("  treat white                     %s\n",TreatmentNames[treatWhite]);
    printf("  treat num                       %s\n",TreatmentNames[treatNum]);
    for (int space=0; space<n_nameSpaces; space++) {
      printf("  treat name.%-20s %s\n",nameSpaceNames[space],
             TreatmentNames[treatName[space]]);
    }
    printf("  treat comment                   %s\n",TreatmentNames[treatComment]);
    printf("  treat string                    %s\n",TreatmentNames[treatString]);
    printf("  treat symbol                    %s\n",TreatmentNames[treatSymbol]);
    printf("  c comments                      %d\n",c_style_comments);
    printf("  asm comments                    %d\n",asm_style_comments);
    printf("  lisp comments                   %d\n",lisp_style_comments);
    printf("\n");
  }

  if (fg->files.size()) {

    fg->ngroups++;
    if (verbose) fprintf(stderr,"\n");

    if (!expectedClones) {
      expectedClones = 3 + (fg->files.size()/20);
    }

    fg->computeCorr();

    fg->printResult();
  }

  delete fg;
}
