/*
   Plagiarism detector. (C) 1994 by Trevor Blackwell, Harvard University
*/

/* 
   Efficient scratch memory system

   Written by Trevor Blackwell, August 1994
*/

class WorkingMemory {
public:
  WorkingMemory();
  ~WorkingMemory();

  char *strdup(char *s);
  void *alloc(int size);
  
  // If you know what you're doing
  char *primalloc(int size, int align, WorkingMemory **cur);

private:
  WorkingMemory *next;
  WorkingMemory *cur;
  
  int allocSoFar;
  char contents[1024];  // this puts an upper bound on how big a chunk you
                       // can allocate.
};

